const electron      = require('electron');
const notifier = require('node-notifier');

var desktopCapturer = electron.desktopCapturer;
const app           = electron.app;
const nativeImage   = electron.nativeImage;
const BrowserWindow = electron.BrowserWindow;
const Tray          = electron.Tray;
const Menu          = electron.Menu;
const ipc           = electron.ipcMain;
// const updater = require("electron-updater");
// const autoUpdater = updater.autoUpdater;
const shell         = electron.shell;

const path          = require('path')
const url           = require('url')

const notify        = require('./lib/notify/notify')

// App
// const mainUrl       = 'http://localhost:3000/';
const mainUrl       = 'https://multi.tellq.io/';
// const mainUrl          = 'https://29efa2fd.ngrok.io';
// const mainUrl          = 'https://omnidemo.tellq.io/';

const appIcon       = path.join(__dirname, 'img/logo.png');

let mainWindow
let tray            = null
let force_quit      = true;
let webrtc_policy   = true;
let notifications   = []; 



// autoUpdater.requestHeaders = { "PRIVATE-TOKEN": "Personal access Token" };
// autoUpdater.autoDownload = true;

// autoUpdater.setFeedURL({
//     provider: "generic",
//     url: "https://gitlab.com/_example_repo_/-/jobs/artifacts/master/raw/dist?job=build"
// });

// autoUpdater.on('checking-for-update', function () {
//     sendStatusToWindow('Checking for update...');
// });

// autoUpdater.on('update-available', function (info) {
//     sendStatusToWindow('Update available.');
// });

// autoUpdater.on('update-not-available', function (info) {
//     sendStatusToWindow('Update not available.');
// });

// autoUpdater.on('error', function (err) {
//     sendStatusToWindow('Error in auto-updater.');
// });

// autoUpdater.on('download-progress', function (progressObj) {
//     let log_message = "Download speed: " + progressObj.bytesPerSecond;
//     log_message = log_message + ' - Downloaded ' + parseInt(progressObj.percent) + '%';
//     log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
//     sendStatusToWindow(log_message);
// });

// autoUpdater.on('update-downloaded', function (info) {
//     sendStatusToWindow('Update downloaded; will install in 1 seconds');
// });

// autoUpdater.on('update-downloaded', function (info) {
//     setTimeout(function () {
//         autoUpdater.quitAndInstall();
//     }, 1000);
// });

// autoUpdater.checkForUpdates();

// function sendStatusToWindow(message) {
//     console.log(message);
// }

function createWindow () {
  mainWindow = new BrowserWindow({
    width: 1280, 
    height: 700,
    minWidth: 400,
    resizable: true,
    title: 'Tellq Multi',
    webSecurity: false,
    autoHideMenuBar: true,
    icon: appIcon,
    plugins: true,
    experimentalFeatures: true,
    nativeWindowOpen: true,
    webPreferences:{
      nodeIntegration: true,
      preload: "./lib/preload.js"
    }
  });
  
  mainWindow.on('page-title-updated', function(e) {
    e.preventDefault()
  });

  mainWindow.webContents.setWebRTCIPHandlingPolicy('default_public_and_private_interfaces');
  mainWindow.loadURL(mainUrl + '?window=app.v0001')
  // mainWindow.webContents.openDevTools();
  // mainWindow.loadURL('index.html')

  // mainWindow.webContents.openDevTools()

  ipc.on('app-resize', function(event, data) {
    console.log("new size: ",data);
    mainWindow.setSize(data.width, data.height, true);
    mainWindow.center();
  });

  ipc.on('chrome-notification', function(event, data) {
    console.log('DATA:', data);
    notifier.notify({
      title: data.title,
      message: data.data.body,
      icon: data.data.icon,
    });
  }); 

  ipc.on('inbound-call', function(event, data) {
    notifications['inbound']  = new notify(mainWindow, 'inbound', data);
     console.log('starting');
   }); 

  ipc.on('inbound-call-answered', function(event, data) {
    if(typeof notifications['inbound'] !== typeof undefined) {
      if (!notifications['inbound'].notifyWindow.isDestroyed()) {
        notifications['inbound'].notifyWindow.close(); 
        notifications['inbound'].notifyWindow.destroy();
        delete notifications['inbound'];
        console.log('answered');
      }
    }
  });
  ipc.on('inbound-call-end', function(event, data) {
    if (typeof notifications['inbound'] !== typeof undefined) {
      if (typeof notifications['inbound'].notifyWindow !== undefined && !notifications['inbound'].notifyWindow.isDestroyed()) {
        notifications['inbound'].notifyWindow.close(); 
        notifications['inbound'].notifyWindow.destroy();
      }
    }
  });

  tray = new Tray(appIcon)
  tray.setToolTip('Tellq application')
  tray.setContextMenu(Menu.buildFromTemplate([
    {label: 'Hide on close', type: 'checkbox', checked: (!force_quit) ? true : false, click: function() {
      force_quit = (force_quit) ? false : true ;
    }},
    {label: 'Debug', click: function() {
      mainWindow.webContents.openDevTools();
    }},
    {label: 'WEBRTC policy', type: 'checkbox', checked: webrtc_policy ? true : false, click: function() {
      webrtc_policy = (webrtc_policy) ? false : true;
      let webrtc_policy_text = webrtc_policy ? 'default_public_and_private_interfaces' : '';

      mainWindow.webContents.setWebRTCIPHandlingPolicy(webrtc_policy_text);
      console.log(mainWindow.webContents.getWebRTCIPHandlingPolicy());
      // 'default_public_and_private_interfaces'
    }},
    {label: 'Quit', accelerator: 'CmdOrCtrl+Q', click: function() {
      force_quit=true; 
      app.quit();
    }},
  ]))
  tray.on('click', () => {
    mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show() ;
  })

  mainWindow.on('close', function(e){
    if(!force_quit){
      e.preventDefault();
      mainWindow.hide();
    } else {
      app.quit()
    }
  });

  mainWindow.on('closed', function () {
    mainWindow = null
    for(var key in notifications){
      if(!notifications[key].notifyWindow.isDestroyed()) {
        notifications[key].notifyWindow.close(); 
        notifications[key].notifyWindow.destroy();
      }
      delete notifications[key];
    }
  });

  // COMMENTED FOR MAC
  // mainWindow.on('show', () => {
  //   tray.setHighlightMode('always')
  // })
  // mainWindow.on('hide', () => {
  //   tray.setHighlightMode('never')
  // })

  // This is the actual solution
  mainWindow.webContents.on("new-window", function(event, url) {
    event.preventDefault();
    shell.openExternal(url);
  });
  //mainWindow.onbeforeunload = function (e) { return false }
}

app.on('ready', createWindow)
app.on('window-all-closed', function () {

  if (process.platform !== 'darwin') {
    app.quit()
  } else {

  };
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
});