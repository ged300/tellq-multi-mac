const electron        = require('electron')
const path            = require('path')
const Positioner      = require('electron-positioner')
const url             = require('url')
const { remote, app } = electron

class Notify {
  constructor (mainWindow, notify_template, data) {
    this.mainWindow = mainWindow;
    this.settings   = {
      BrowserWindow: {
        width: 700, 
        height: 180,
        resizable: false,
        title: 'Tellq Multi',
        webSecurity: false,
        autoHideMenuBar: true,
        movable: false,
        minimizable: false,
        maximizable: false,
        skipTaskbar: true,
        closable: false,
        alwaysOnTop: true,
        frame: false,
        //parent: this.mainWindow,
        show: false,
        transparent: true
      },
      padding_x: 0,
      padding_y: 15
    };
    this.data = data;
    this.createWindow(notify_template);
  }

  createWindow(notify_template)
  {
    if (process.type === 'renderer') {
      this.BrowserWindow  = electron.remote.BrowserWindow;
      this.ipc            = electron.ipcRenderer;
    } else {
      this.BrowserWindow  = electron.BrowserWindow;
      this.ipc            = electron.ipcMain;
    }

    const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize

    this.notifyWindow     = new this.BrowserWindow(this.settings.BrowserWindow);
    this.notifyWindow.setPosition( 
      (width - this.settings.BrowserWindow.width - this.settings.padding_x),
      (height - this.settings.BrowserWindow.height - this.settings.padding_y)
    );
    this.notifyWindow.loadURL(url.format({
      pathname: path.join(__dirname, '/render/' + notify_template + '.html'),
      protocol: 'file:',
      slashes: true
    }))
    
    let calling_to = 'Calling to: ' + this.data.phoneline;

    if (this.data.queue != '') {
      calling_to += ' (' + this.data.queue + ')';
    }

    this.notifyWindow.webContents.executeJavaScript('document.getElementById("contact-name").innerHTML = "' + this.data.contact + ' (' + this.data.phone + ')";' +
      'document.getElementById("phoneline-queue").innerHTML ="' + calling_to + '";' , function (result) {
      console.log(result)
    })

    this.notifyWindow.once('ready-to-show', () => {
      this.notifyWindow.show()
    })

    var self  = this;

    this.notifyWindow.on('focus',function(){
      if(!self.mainWindow.isDestroyed()) {
        self.mainWindow.focus();
      }
    });

    this.ipc.on('notify-close', function(event, data) {
      self.notifyWindow.close();
      self.notifyWindow.destroy();
    });
    this.ipc.on('notify-call-action', function(event, data) {
      if(!self.mainWindow.isDestroyed()) {
        if(data.type == 'answer') {
          self.mainWindow.webContents.executeJavaScript('Workplace._phone.Answer();');
        } 
        if(data.type == 'hangup') {
          self.mainWindow.webContents.executeJavaScript('Workplace._phone.Hangup();');
        }
      }
      if(typeof self.notifyWindow != undefined && !self.notifyWindow.isDestroyed()) {
        self.notifyWindow.close();
        self.notifyWindow.destroy();
      }
    });
  }
}

module.exports = Notify
