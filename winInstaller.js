const electronInstaller = require('electron-winstaller');


try {
    electronInstaller.createWindowsInstaller({
      appDirectory: 'release-builds/Tellq-win32-ia32/',
      outputDirectory: 'dist/build/installer64',
      authors: 'Tellq.',
      exe: './Tellq-setup-0.2.exe'
    });
    console.log('It worked!');
  } catch (e) {
    console.log(`No dice: ${e.message}`);
  }